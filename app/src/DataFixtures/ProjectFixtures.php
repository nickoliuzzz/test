<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProjectFixtures extends Fixture
{
    const PROJECT_COUNT = 5;
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < self::PROJECT_COUNT; $i++)
        {
            $manager->persist($this->createProject());
        }

        $manager->flush();
    }



    private function createProject(): Project
    {
        $project = new Project();

        $project
            ->setCode('project')
            ->setName('Project')
            ->setBudget(100)
            ->setUrl('http://example.com/my-page')
        ;

        return $project;
    }
}
