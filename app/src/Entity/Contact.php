<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("project")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("project")
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("project")
     */
    private string $lastName;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups("project")
     */
    private string $phone;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="contacts")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}