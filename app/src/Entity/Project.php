<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("project")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("project")
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups("project")
     */
    private string $code = '';

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("project")
     */
    private string $url;

    /**
     * @ORM\Column(type="float")
     * @Groups("project")
     */
    private float $budget;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="project", orphanRemoval=true,cascade={"persist"})
     * @Groups("project")
     */
    private iterable $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        if ($this->code) {
            throw new \Exception('Code can\'t be changed.');
        }

        $this->code = $code;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getBudget(): float
    {
        return $this->budget;
    }

    public function setBudget(float $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setProject($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getProject() === $this) {
                $contact->setProject(null);
            }
        }

        return $this;
    }
}