<?php

namespace App\Controller;

use App\DTO\ProjectFilterDTO;
use App\Entity\Project;
use App\Form\ProjectFilterType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Utils\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/projects")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route(methods={"GET"})
     */
    public function list(Request $request, ProjectRepository $repository)
    {
        try {
            $projecFilterDTO = new ProjectFilterDTO();
            $form = $this->createForm(ProjectFilterType::class, $projecFilterDTO)
                ->submit($request->query->all());

            if (!$form->isValid()) {
                return (new Response('Bad parameters.'))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            return new Response(Serializer::serialize($repository->findByParameters($projecFilterDTO), 'project'));
        } catch (\Throwable $exception) {
            return (new Response($exception->getMessage()))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * @Route("/{project}",methods={"GET"})
     */
    public function getItem(Project $project)
    {
        return new Response(Serializer::serialize($project, 'project'));
    }

    /**
     * @Route(methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $form = $this->createForm(ProjectType::class)->submit(json_decode((string) $request->getContent(), true));

            if (!$form->isValid()) {
                return (new Response('Bad parameters.'))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            $entityManager->persist($form->getData());
            $entityManager->flush();

            return new Response(Serializer::serialize($form->getData(), 'project'));
        } catch (\Throwable $exception) {
            return (new Response($exception->getMessage()))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/{project}",methods={"PATCH"})
     */
    public function update(Project $project, Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $form = $this->createForm(ProjectType::class, $project)->submit(json_decode((string) $request->getContent(), true), false);

            if (!$form->isValid()) {
                return (new Response('Bad parameters.'))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            $entityManager->flush();

            return new Response(Serializer::serialize($form->getData(), 'project'));
        } catch (\Throwable $exception) {
            return (new Response($exception->getMessage()))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/{project}",methods={"DELETE"})
     */
    public function delete(Project $project, EntityManagerInterface $entityManager)
    {
        try {
            $entityManager->remove($project);
            $entityManager->flush();

            return new Response();
        } catch (\Throwable $exception) {
            return (new Response($exception->getMessage()))->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }
}
