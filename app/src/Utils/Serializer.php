<?php

namespace App\Utils;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

class Serializer
{
    public static function serialize($data, string $groups = null): string
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];

        $serializer = new SymfonySerializer($normalizers, $encoders);

        $context = [];
        if ($groups) {
            $context['groups'] = $groups;
        }

        return $serializer->serialize($data, 'json', $context);
    }

}