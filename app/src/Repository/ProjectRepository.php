<?php

namespace App\Repository;

use App\DTO\ProjectFilterDTO;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findByParameters(ProjectFilterDTO $projectFilterDTO)
    {
        $queryBuilder =  $this->createQueryBuilder('p');

        $this->addBudgetFilter($queryBuilder, $projectFilterDTO);
        $this->addCodeFilter($queryBuilder, $projectFilterDTO);

        return $queryBuilder->getQuery()->getResult();
    }

    private function addBudgetFilter(QueryBuilder $queryBuilder, ProjectFilterDTO $projectFilterDTO)
    {
        if ($projectFilterDTO->minBudget && $projectFilterDTO->maxBudget) {
            $queryBuilder->andWhere($queryBuilder->expr()->between(
                'p.budget',
                $projectFilterDTO->minBudget,
                $projectFilterDTO->maxBudget
            ));
        }
    }

    private function addCodeFilter(QueryBuilder $queryBuilder, ProjectFilterDTO $projectFilterDTO)
    {
        if ($projectFilterDTO->code) {
            $queryBuilder->andWhere($queryBuilder->expr()->like(
                'p.code',
                ":code"
            ))
                ->setParameter('code', "%{$projectFilterDTO->code}%")
            ;
        }
    }


}
