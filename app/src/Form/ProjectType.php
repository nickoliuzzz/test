<?php

namespace App\Form;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Url;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 5, 'max' => 50]),
                    new Regex(['pattern' => '/^[A-Za-z\s]+$/']),
                ],
            ])
            ->add('code', TextType::class, [
                'by_reference' => false,
                'constraints' => [
                    new Length(['min' => 3, 'max' => 10]),
                    new Regex(['pattern' => '/^[a-z]+$/']),
                ],
            ])
            ->add('url', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Regex(['pattern' => "/(http:\/\/|https:\/\/){1}([a-z0-9]+\.)*(example\.com)+(:[0-9]{)?(\/.*)?/"]),
                ],
            ])
            ->add('budget', NumberType::class)
            ->add('contacts', CollectionType::class, [
                'entry_type' => ContactType::class,
                'by_reference' => true,
                'allow_add' => true,
                'constraints' => [
                    new Count(['min' => 1]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
