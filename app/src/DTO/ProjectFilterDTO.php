<?php

namespace App\DTO;

class ProjectFilterDTO
{
    public ?string $code = null;
    public ?float $minBudget = null;
    public ?string $maxBudget = null;
}