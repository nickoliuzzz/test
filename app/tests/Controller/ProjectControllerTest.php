<?php

namespace App\Tests\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Prophecy\Prophecy\MethodProphecy;
use Reflection;
use ReflectionProperty;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectControllerTest extends WebTestCase
{
    const PROJECT_FIELDS = [
        'id',
        'name',
        'code',
        'url',
        'budget',
        'contacts',
    ];

    const DATA_FOR_UPDATE = '{"name": "New Name"}';
    const DATA_FOR_CREATE = '
        {
          "name": "New Project",
          "code": "ewrojec",
          "url": "http://example.com/new-page",
          "budget": 100,
          "contacts": [
            {
              "firstName": "Jane",
              "lastName": "Doe",
              "phone": "+012 (34) 567-89-10"
            }   
          ]
        }
    ';
    private KernelBrowser $client;
    private EntityManagerInterface $manager;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->disableReboot();
        $this->manager = self::$container->get('doctrine')->getManager();
        $this->manager->beginTransaction();
    }

    protected function tearDown()
    {
        $this->manager->rollback();
        $this->manager->close();
    }

    private function getClientResponse(): array
    {
        if ($this->client->getResponse()->getContent() === false) {
            throw new Exception('Responce not getted.');
        }

        return json_decode($this->client->getResponse()->getContent(), true);
    }

    private function checkFields(array $response, array $neededFields)
    {
        foreach ($neededFields as $field) {
            $this->assertArrayHasKey($field, $response);
        }
    }

    private function getProjectForTest(): Project
    {
        $projects = $this->manager->getRepository(Project::class)->findAll();
        $project = array_pop($projects);

        if (!$project instanceof Project) {
            throw new Exception('No data for test.');
        }

        return $project;
    }

    private function getProjectsCount(): int
    {
        return count($this->manager->getRepository(Project::class)->findAll());
    }

    public function testList()
    {
        $url = '/projects';

        $this->client->request(Request::METHOD_GET, $url);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $responce = $this->getClientResponse();
        $this->checkFields(array_pop($responce), self::PROJECT_FIELDS);
    }

    public function testGet()
    {
        $url = '/projects/%d';

        $this->client->request(Request::METHOD_GET, sprintf($url, $this->getProjectForTest()->getId()));
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $responce = $this->getClientResponse();
        $this->checkFields($responce, self::PROJECT_FIELDS);
    }

    public function testDelete()
    {
        $url = '/projects/%d';

        $projectId = $this->getProjectForTest()->getId();

        $this->client->request(Request::METHOD_DELETE, sprintf($url, $projectId));
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->client->request(Request::METHOD_DELETE, sprintf($url, $projectId));
        self::assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function testUpdate()
    {
        $url = '/projects/%d';

        $project = $this->getProjectForTest();
        $projectName = $project->getName();

        $this->client->request(Request::METHOD_PATCH, sprintf($url, $project->getId()), [], [], [], self::DATA_FOR_UPDATE);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        self::assertNotEquals($projectName, $project->getName());
    }

    public function testCreate()
    {
        $url = '/projects';

        $count = $this->getProjectsCount();
        $this->client->request(Request::METHOD_POST, $url, [], [], [], self::DATA_FOR_CREATE);
        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertEquals($count + 1, $this->getProjectsCount());
    }
}